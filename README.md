# Introduction

Simple Elasticsearch role to deploy a cluster of nodes

# Usage

```yaml
---
- hosts: logs
  become: yes
  remote_user: admin
  vars:
    clustername: logcluster
    elasticsearch_port: 9200
    node_data: true
    node_master: true
    kibana_port: 5601
    kibana_host: 0.0.0.0
    kibana_public_url: http://kibana.domain.tld:{{ kibana_port }} (default to http://{{ ansible_host }}:{{ kibana_port }})
    pathdata: /var/lib/elasticsearch
    pathlogs: /var/log/elasticsearch
    pathrepo: /srv
    seed_hosts:
      - logs01
      - logs02
      - logs03
  roles:
    - logserver
```
